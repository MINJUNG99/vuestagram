import { createStore } from "vuex";
import axios from "axios";

const store = createStore({
  state() {
    return {
      // 원하는 데이터 저장
      // state: 여러 컴포넌트 간에 공유할 데이터 뷰의 데이터랑 똑같이 사용하는데, 여러 컴포넌트에 데이터가 공유가 된다는 점이 다르다.
      name: "kim", // 해당 데이터는 변수가 맞으나 보통 state로 정의한다. (상태관리)
      age: 20,
      likes: [0, 0, 0],
      isClickLikes: [false, false, false],
      more: {},
    };
  },
  mutations: {
    // state는 상단의 state object를 뜻한다
    updateName(state) {
      state.name = "park";
    },
    updateLikes(state, payload) {
      state.likes[payload.idx] += payload.like;
    },
    addAge(state) {
      state.age++;
    },
    addLikes(state, payload) {
      state.likes[payload]++;
      state.isClickLikes[payload] = true;
    },
    minusLikes(state, payload) {
      state.likes[payload]--;
      state.isClickLikes[payload] = false;
    },
    setMore(state, payload){
        state.more = payload;
    }
  },
  actions: {
    // ajax, or something do for long times
    getData(context) {
      const url = `https://codingapple1.github.io/vue/more0.json`;
      axios
        .get(url)
        .then((res) => {
          console.log("res", res.data);
          context.commit('setMore',res.data);
          return true;
        })
        .catch((err) => {
          console.log("error:::", err);
        });
    },
  },
});

export default store;
