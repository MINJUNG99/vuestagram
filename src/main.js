import { createApp } from 'vue'
import App from './App.vue'
import mitt from 'mitt';    // mitt 라이브러리 등록 (./가 없으면 라이브러리 import)
import axios from 'axios';
import store from './store';    // 
import './registerServiceWorker'

let emitter = mitt();
let app = createApp(App);   // vue 의 설정부분을 다루는 변수라고 보면 된다.

// global 변수보관함 {emitter: emitter}추가
// 자주쓰는 라이브러리 같은 경우 여기다가 등록하는 사례가 많음 (ex. axios)
// this.axios로 호출 가능

// app.config.globalProperties.$axios = axios;
app.config.globalProperties.$axios = axios;
app.config.globalProperties.emitter = emitter;  // 모든 컴포넌트가 emitter을 사용할 수 있게 하고 싶어요

// 설정한 변수로 등록
// use(store) store에 저장한 state를 전역변수로 사용하겠습니다.
app.use(store).mount('#app')
